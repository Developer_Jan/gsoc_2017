package za.ac.sun.cs.green.service.julia;

import za.ac.sun.cs.green.expr.IntVariable;
import java.util.ArrayList;
import java.util.List;

/**
 * @date: 2017/06/23
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 * Supervisor: Jaco Geldenhuys
 *
 * Description:
 * Repository interface for caching.
 */
public interface Repo {

    public void add(RepoEntry entry);

    public int size();

    public List<RepoEntry> extract(Double SATDelta, ArrayList<IntVariable> variables, int k);

}
