package za.ac.sun.cs.green.service.julia;

import za.ac.sun.cs.green.expr.IntVariable;

import java.util.*;

/**
 * @date: 2017/06/19
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 * Supervisor: Jaco Geldenhuys
 *
 * Description:
 * Storage unit for the possible reusable models of the satisfied expressions.
 *
 */
public class SatRepo implements Repo {

    /**
     * Contains the entries in the repo.
     */
    private List<RepoEntry> entries;

    public SatRepo() {
        this.entries = new ArrayList<RepoEntry>();
    }

    /**
     * To add an entry to the repo.
     *
     * @param entry the entry to be added.
     */
    public void add(RepoEntry entry) {
        this.entries.add(entry);
    }

    /**
     * @return the number of entries in the repo.
     */
    public int size() {
        return this.entries.size();
    }

    /**
     * To test if the two objects are of the same size.
     *
     * @param a size of new model
     * @param desiredSize desired model size
     * @return if a is the same size as b
     */
    private boolean isValid(int a, int desiredSize) {
        return a >= desiredSize;
    }

    /**
     * Returns k entries closest to the given SATDelta.
     *
     * @author Andrea Aquino
     * @param SATDelta the given SATDelta to use as reference for filtering
     * @param variables a given list of variables used in the expression
     * @param k the number of entries to extract
     * @return the filtered entries, sorted by increasing distance from the given SATDelta.
     */
    private List<RepoEntry> filterByProximity(Double SATDelta, ArrayList<IntVariable> variables, int k) {
        PriorityQueue<Pair<Double, RepoEntry>> queue = new PriorityQueue<>(
                k,
                (p1, p2) -> ((-1) * (p1.getL().compareTo(p2.getL()))));

        // Load the first k entries in the queue, then keep updating the queue
        // inserting elements whose distance from satDelta is smaller than the
        // distance of the maximum element in the queue. The maximum is removed
        // whenever a new elements is inserted so that the overall complexity
        // of this method is O(n*log(k)).
        int i = 0;
        for (RepoEntry entry : this.entries) {
            if (!isValid(entry.getMapping().size(), variables.size())) {
                // Entries containing models with less variables than
                // the reference expression are immediately discarded.
                continue;
            }

            Double delta = Math.abs(entry.getSATDelta()-SATDelta);
            if (i < k) {
                queue.add(new Pair<>(delta, entry));
            } else {
                Pair<Double, RepoEntry> head = queue.peek();
                if (delta.compareTo(head.getL()) < 0) {
                    queue.poll();
                    queue.add(new Pair<>(delta, entry));
                }
            }
            i++;
        }

        List<RepoEntry> closest = new ArrayList<>(k);
        while (!queue.isEmpty()) {
            closest.add(queue.remove().getR());
        }
        Collections.reverse(closest);
        return closest;
    }

    /**
     * Returns k entries closest to the given SATDelta.
     *
     * @param SATDelta the given SATDelta to use as reference for filtering
     * @param variables a given list of variables used in the expression
     * @param k the number of entries to extract
     * @return the extracted entries, sorted by increasing distance from the given SATDelta.
     */
    public List<RepoEntry> extract(Double SATDelta, ArrayList<IntVariable> variables, int k) {
        if (k <= 0) {
            return new ArrayList<RepoEntry>();
        } else {
            return this.filterByProximity(SATDelta, variables, k);
        }
    }
}
