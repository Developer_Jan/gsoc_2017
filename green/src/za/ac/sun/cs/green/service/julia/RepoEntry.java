package za.ac.sun.cs.green.service.julia;

import za.ac.sun.cs.green.expr.IntVariable;

import java.util.Map;

/**
 * @date: 2017/06/19
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 * Supervisor: Jaco Geldenhuys
 *
 * Description:
 * Entry stored in the SatRepo.
 */
public class RepoEntry {

    /**
     * The SAT-Delta value
     */
    private Double SATDelta;

    /**
     * The mapping of each variable to its corresponding value.
     */
    private Map<IntVariable, Object> mapping;

    public RepoEntry(Double SATDelta, Map<IntVariable, Object> mapping) {
        this.SATDelta = SATDelta;
        this.mapping = mapping;
    }

    /**
     * Getter method for the SATDelta value.
     *
     * @return the SATDelta value of the entry.
     */
    public Double getSATDelta() {
        return SATDelta;
    }

    /**
     * Getter method for the mapping of the variables.
     *
     * @return a map of the variables.
     */
    public Map<IntVariable, Object> getMapping() {
        return mapping;
    }

    @Override
    public String toString() {
        return String.format(
                "Entry(SATDelta=%s, {variable, value}=%s)",
                SATDelta,
                mapping.toString());
    }
}
