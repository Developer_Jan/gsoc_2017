package za.ac.sun.cs.green.service.z3;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;

import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.service.smtlib.SATSMTLIBService;

public class SATZ3Service extends SATSMTLIBService {

	private final String DEFAULT_Z3_PATH;

	private final String DEFAULT_Z3_ARGS = "-smt2 -in";
	
	private final String z3Command;
	
	public SATZ3Service(Green solver, Properties properties) {
		super(solver);
		String z3Path = "";
        InputStream is = SATZ3Service.class.getClassLoader()
                .getResourceAsStream("green/build.properties");
        if (is != null) {
            Properties p = new Properties();
            try {
                p.load(is);
                z3Path = p.getProperty("z3path");
            } catch (IOException e) {
                // do nothing
            }
        }

        DEFAULT_Z3_PATH = z3Path;
		String p = properties.getProperty("green.z3.path", DEFAULT_Z3_PATH);
		String a = properties.getProperty("green.z3.args", DEFAULT_Z3_ARGS);
		z3Command = p + ' ' + a;
	}

	@Override
	protected Boolean solve0(String smtQuery) {
		String output = "";
		try {
            Process process = Runtime.getRuntime().exec(z3Command);
            OutputStream stdin = process.getOutputStream();
            InputStream stdout = process.getInputStream();
            BufferedReader outReader = new BufferedReader(new InputStreamReader(stdout));
            stdin.write((smtQuery + "(exit)\n").getBytes());
            stdin.flush();
            stdin.close();
            output = outReader.readLine();
            stdout.close();
            process.destroy();
		} catch (IOException x) {
			log.log(Level.SEVERE, x.getMessage(), x);
		}
        if (output.equals("sat")) {
            return true;
        } else if (output.equals("unsat")) {
            return false;
        } else {
            log.severe("Z3 returned a null" + output) ;
            return null;
        }
	}

}
