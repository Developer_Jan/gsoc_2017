package za.ac.sun.cs.green.service.julia;

import za.ac.sun.cs.green.expr.*;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.service.SATService;
import za.ac.sun.cs.green.util.Configuration;
import za.ac.sun.cs.green.util.Reporter;

import java.util.*;
import java.util.logging.Level;

/**
 * @date: 2017/06/02
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 *
 * Description:
 * Utopia (a SMT caching framework) is defined in the paper:
 * "Heuristically Matching Formula Solution Spaces To Efficiently Reuse Solutions"
 * published at the International Conference on Software Engineering (ICSE'17)
 * by Andrea Aquino, Giovanni Denaro and Mauro Pezze'.
 *
 * Julia (Java version of Utopia Linear Integer Arithmetic)
 * re-implemented to improve GREEN. Julia is implemented
 * as a service in GREEN.
 *
 */
public class JuliaService extends SATService {

    /*
     * The number of closest entries to extract
     */
    private final int Ks = 10;

    /*
     * Contains the model to use for the expression evaluator
     */
    protected static Map<IntVariable, Object> MODEL_MAPPING;

    /*
     * Keep track of all the variables in a formula.
     * The use is for look-ups in the MODEL_MAPPING
     * for the expression evaluator.
     */
    protected static ArrayList<IntVariable> VARS;

    /*
     * Stores data of satisfiable formulas.
     */
    private SatRepo SAT_REPO;

    /*
     * Instance of Green to do model checking.
     */
    protected Green modelChecker;

    /**
     * The value of the reference solution.
     * For experiments: -10000, 0, 100
     */
    private static final Integer[] REFERENCE_SOLUTION = {-10000, 0, 100};

    /*
     * A counter to keep track of which REFERENCE_SOLUTION
     * currently working with. This is for JuliaVisitor.
     */
    protected static int COUNTER;

    /*
     * For debugging purposes.
     */
    public static final boolean DEBUG = false;

    /*
     ##################################################################
     #################### For logging purposes ########################
     ##################################################################
    */

    /**
     * Number of times the slicer has been invoked.
     */
    private int invocationCount = 0;

    /**
     * Number of times some model satisfied some expression.
     */
    private int satModelCount = 0;

    /**
     * Number of times some model did not satisfy some expression.
     */
    private int unsatModelCount = 0;

    /**
     * Total number of times the SMT solver was called.
     */
    private int solverCount = 0;

    /**
     * Total number of models cached.
     */
    private int entryCount = 0;

    /**
     * Total number of satisfied expressions.
     */
    private int satCount = 0;

    /**
     * Total number of unsatisfied expressions.
     */
    private int unsatCount = 0;

    /**
     * Execution Time of the service.
     */
    private long timeConsumption = 0;

    /**
     * Total number of times a valid entry found in the satRepo.
     */
    private int cacheHitCount = 0;

    /**
     * Total number of a valid entry was not found in the satRepo.
     */
    private int cacheMissCount = 0;

    /**
     * Total number of variables encountered.
     */
    private int totalVariableCount = 0;

    /**
     * To keep track of the already seen variables.
     */
    protected static ArrayList<IntVariable> newVariables;

    protected static ArrayList<Double> satDeltaValues;

    protected static ArrayList<Double> satDeltaValuesInRepo;

    /**
     * Total number of new variables encountered.
     */
    protected static int newVariableCount = 0;

    /*##################################################################*/

    /**
     * Constructor for the basic service. It simply initializes its three
     * attributes.
     *
     * Julia Service recommends to run with Factorize or Canonize Service.
     *
     * @param solver the {@link Green} solver this service will be added to
     */
    public JuliaService(Green solver) {
        super(solver);
        SAT_REPO = new SatRepo();
        newVariables = new ArrayList<IntVariable>();
        satDeltaValues = new ArrayList<Double>();
        satDeltaValuesInRepo = new ArrayList<Double>();

        // Initializes and configure Green instance for model checking.
        modelChecker = new Green();

        Properties props = new Properties();
        props.setProperty("green.services", "solver");
        props.setProperty("green.service.solver", "(bounder z3java)");
        props.setProperty("green.service.solver.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
        props.setProperty("green.service.solver.z3java", "za.ac.sun.cs.green.service.z3.ModelZ3JavaService");

        Configuration config = new Configuration(modelChecker, props);
        config.configure();
    }

    protected static Integer getReferenceSolution(int a) {
        return REFERENCE_SOLUTION[a];
    }

    @Override
    protected Boolean solve(Instance instance) {
        long startTime = System.currentTimeMillis();
        Boolean SAT;

        SAT = solve0(instance);
        timeConsumption += System.currentTimeMillis() - startTime;

        return SAT;
    }

    private Boolean solve0(Instance instance) {
        Double result;
        Boolean SAT; // final result to return -- if instance is SAT/UNSAT
        VARS = new ArrayList<IntVariable>();
        Expression expr = instance.getFullExpression();

        invocationCount++;
        result = calculateSATDelta(expr);

        SAT = utopiaAlgorithm(result, expr);

        if (SAT) {
            // if model satisfied expression
            // return immediately
            return true;
        } // else continue and calculate solution

        // call solver & store
        SAT = solver(result, expr);

        return  SAT;
    }

    /**
     * Calculates the average SATDelta value of a given Expression.
     *
     * @param expr the given Expression.
     * @return the average SATDelta value.
     */
    private Double calculateSATDelta(Expression expr) {
        Double result = 0.0;
        JuliaVisitor jVisitor = new JuliaVisitor();

        try {

            for (COUNTER = 0; COUNTER < REFERENCE_SOLUTION.length; COUNTER++) {
                expr.accept(jVisitor);
                result = jVisitor.getResult(); // use SAT-Delta for table
            }

            // calculate average SAT-Delta
            result = result/REFERENCE_SOLUTION.length;
            satDeltaValues.add(result);
            totalVariableCount += VARS.size();

        } catch (VisitorException x) {
            log.log(Level.SEVERE, "encountered an exception -- this should not be happening!", x);
        }

        return result;
    }

    /**
     * Finds reusable models for the given Expression from the given SATDelta
     * value.
     *
     * @param satDelta the SATDelta value for the model filtering
     * @param expr the Expression to solve
     * @return SAT - if the Expression could be satisfied from previous models
     */
    private Boolean utopiaAlgorithm(Double satDelta, Expression expr) {
        /*
         * Check if SAT-Delta is in table
         * If in table -> test if model satisfies
         * If not take next (k) closest SAT-Delta
         * If not satisfied, call solver
         */
        if (SAT_REPO.size() != 0) {

            List<RepoEntry> temp = SAT_REPO.extract(satDelta, VARS, Ks);

            if (temp.size() == 0) {
                cacheMissCount++;
                return false;
            } else{
                cacheHitCount++;
            }

            for (RepoEntry entry : temp) {
                // extract model
                MODEL_MAPPING = entry.getMapping();
                // test model satisfiability
                JuliaExpressionEvaluator exprSATCheck = new JuliaExpressionEvaluator();

                try {
                    expr.accept(exprSATCheck);
                } catch (VisitorException x) {
                    log.log(Level.SEVERE, "encountered an exception -- this should not be happening!", x);
                }

                if (exprSATCheck.isSat()) {
                    // already in repo,
                    // don't have to do anything
                    satModelCount++;
                    satCount++;

                    return true;
                } else {
                    // call solver
                    unsatModelCount++;
                }
            }
        } else {
            // repo empty -> call solver
            cacheMissCount++;
        }

        return false;
    }

    /**
     * Calls the model checker to solve the Expression.
     * If the Expression could be satisfied, its model and SATDelta value is stored.
     *
     * @param satDelta the SATDelta value to store as key.
     * @param expr the Expression to store
     * @return SAT - if the Expression could be satisfied.
     */
    private Boolean solver(Double satDelta, Expression expr) {
        // Get model for formula
        Boolean SAT;
        Instance i = new Instance(modelChecker, null, expr);
        solverCount++;

        Object result_t = i.request("solver");
        if (result_t != null) {
            // is feasible
            @SuppressWarnings("unchecked")
            Map<IntVariable, Object> mapping = (Map<IntVariable, Object>) result_t;
            RepoEntry tempEntry = new RepoEntry(satDelta, mapping);

            SAT_REPO.add(tempEntry);
            satDeltaValuesInRepo.add(satDelta);
            entryCount++;

            satCount++;
            SAT = true;
        } else {
            unsatCount++;
            SAT = false;
        }

        return SAT;
    }

    /**
     * Display the list of values as a histogram.
     *
     * @param reporter the output reporter
     * @param list the list containing values of type Double
     */
    private void displayAsHistogram(Reporter reporter, ArrayList<Double> list) {
        HashMap<Double, Integer> histogram = new HashMap<Double, Integer>();

        for (Double x : list) {
            histogram.merge(x, 1, (a, b) -> a + b);
        }

        reporter.report(getClass().getSimpleName(), histogram.toString());
    }

    /**
     * Calculates the distribution of the SATDelta values, for the reporter.
     * @param reporter the reporter
     * @param list SATDelta values
     */
    private void distribution(Reporter reporter, ArrayList<Double> list) {
        Double avg = 0.0;
        Collections.sort(list);

        reporter.report(getClass().getSimpleName(),"minSATDelta = " + list.get(0));
        reporter.report(getClass().getSimpleName(),"maxSATDelta = " + list.get(list.size()-1));

        for (Double x : list) {
            avg += x;
        }

        avg = avg/list.size();

        reporter.report(getClass().getSimpleName(),"meanSATDelta = " + avg);

        Double sum = 0.0;

        for (Double x : list) {
            sum += Math.pow((x - avg), 2);
        }

        Double sigma = sum/(list.size()-1);
        sigma = Math.sqrt(sigma);

        reporter.report(getClass().getSimpleName(),"standard deviation of SATDelta = " + sigma);

        Double cv = sigma/avg;

        reporter.report(getClass().getSimpleName(),"coefficient of variation of SATDelta = " + cv);

    }

    @Override
    public void report(Reporter reporter) {
        reporter.report(getClass().getSimpleName(), "invocations = " + invocationCount);
        reporter.report(getClass().getSimpleName(), "totalVariables = " + totalVariableCount);
        reporter.report(getClass().getSimpleName(), "totalNewVariables = " + newVariableCount);
        reporter.report(getClass().getSimpleName(), "totalOldVariables = " + (totalVariableCount-newVariableCount));
        reporter.report(getClass().getSimpleName(), "SAT queries = " + satCount);
        reporter.report(getClass().getSimpleName(), "UNSAT queries = " + unsatCount);
        reporter.report(getClass().getSimpleName(), "cacheHitCount = " + cacheHitCount);
        reporter.report(getClass().getSimpleName(), "cacheMissCount = " + cacheMissCount);
        reporter.report(getClass().getSimpleName(), "Models reused = " + satModelCount);
        reporter.report(getClass().getSimpleName(), "Models tested unsat = " + unsatModelCount);
        reporter.report(getClass().getSimpleName(), "number of times SMT solver was called = " + solverCount);
        reporter.report(getClass().getSimpleName(), "SAT entries added to cache = " + entryCount);

        reporter.report(getClass().getSimpleName(), "totalSatDeltaValues distribution: ");
        distribution(reporter, satDeltaValues);
        reporter.report(getClass().getSimpleName(), "SatDeltaValues in Repo distribution: ");
        distribution(reporter, satDeltaValuesInRepo);

        reporter.report(getClass().getSimpleName(), "Display SAT-Delta as histogram: ");
        displayAsHistogram(reporter, satDeltaValues);
        reporter.report(getClass().getSimpleName(), "Display SAT-Delta (in repo) as histogram: ");
        displayAsHistogram(reporter, satDeltaValuesInRepo);

        reporter.report(getClass().getSimpleName(), "timeConsumption = " + timeConsumption);
    }
}

class JuliaVisitor extends Visitor {

    /*
     * Local stack to calculate the SAT-Delta value
     */
    private Stack<Integer> stack = new Stack<Integer>();

    /**
     * @return x - SAT-Delta value
     */
    public Double getResult() {
        Double x = 0.0;
        x += stack.pop();
        return x;
    }

    @Override
    public void postVisit(Expression expression) throws VisitorException {
        super.postVisit(expression);
    }

    @Override
    public void postVisit(Variable variable) throws VisitorException {
        super.postVisit(variable);

        if (!JuliaService.VARS.contains((IntVariable) variable)) {
            // add the unique variables to the list
            JuliaService.VARS.add((IntVariable) variable);
        }

        if (!JuliaService.newVariables.contains((IntVariable) variable)) {
            JuliaService.newVariables.add((IntVariable) variable);
            JuliaService.newVariableCount++;
        }

        Integer value = JuliaService.getReferenceSolution(JuliaService.COUNTER);
        stack.push(value);
    }

    @Override
    public void postVisit(IntConstant constant) throws VisitorException {
        super.postVisit(constant);
        stack.push(constant.getValue());
    }

    @Override
    public void postVisit(Operation operation) throws VisitorException {
        super.postVisit(operation);
        SATDelta(operation, stack);
    }

    /**
     * Calculates the SAT-Delta value for a given oparation and
     * pushes the result to a given stack.
     *
     * The distance of an expression from a set of reference models is called
     * "SatDelta" and is defined in the paper:
     * "Heuristically Matching Formula Solution Spaces To Efficiently Reuse Solutions"
     * published at the International Conference on Software Engineering (ICSE'17)
     * by Andrea Aquino, Giovanni Denaro and Mauro Pezze'.
     *
     * @param operation the current operation working with
     * @param stack the stack to push the result to
     */
    private void SATDelta(Operation operation, Stack<Integer> stack) {
        Integer l = null;
        Integer r = null;

        int arity = operation.getOperator().getArity();
        if (arity == 2) {
            if (!stack.isEmpty()) {
                r = stack.pop();
            }
            if (!stack.isEmpty()) {
                l = stack.pop();
            }
        } else if (arity == 1) {
            if (!stack.isEmpty()) {
                l = stack.pop();
            }
        }

        Operation.Operator op = operation.getOperator();

        switch (op) {
            case MUL:
                stack.push(l * r);
                break;
            case ADD:
                stack.push(l + r);
                break;
            case SUB:
                stack.push(l - r);
                break;
            case OR:
            case AND:
                stack.push(l + r);
                break;
            case LE:
            case EQ:
            case GE:
                stack.push(Math.abs(l - r));
                break;
            case LT:
            case NE:
            case GT:
                stack.push(Math.abs(l - r) + 1);
                break;
            default:
                break;
        }
    }
}

class JuliaExpressionEvaluator extends Visitor {

    /*
     * Local stack for the evaluation of the expression.
     */
    private Stack<Object> evalStack = new Stack<Object>();

    /**
     * Public method to get the satisfiability status of the
     * expression.
     *
     * @return SAT - true if the expression is satisfied,
     *             - false otherwise
     */
    public Boolean isSat() {
        return (Boolean) evalStack.pop();
    }

    @Override
    public void postVisit(Expression expression) throws VisitorException {
        super.postVisit(expression);
    }

    @Override
    public void postVisit(Variable variable) throws VisitorException {
        super.postVisit(variable);

        int pos = JuliaService.VARS.indexOf((IntVariable) variable);
        Integer value = 0;
        int i = 0;
        for (IntVariable key : JuliaService.MODEL_MAPPING.keySet()) {

            if (i > pos) {
                throw new NullPointerException();
            }

            if (i == pos) {
                value = (Integer) JuliaService.MODEL_MAPPING.get(key);
                break;
            }
            i++;
        }

        evalStack.push(value);
    }

    @Override
    public void postVisit(IntConstant constant) throws VisitorException {
        super.postVisit(constant);
        evalStack.push(constant.getValue());
    }

    @Override
    public void postVisit(Operation operation) throws VisitorException {
        super.postVisit(operation);

        Boolean SAT = false;
        Object l = null;
        Object r = null;

        int arity = operation.getOperator().getArity();
        if (arity == 2) {
            if (!evalStack.isEmpty()) {
                r = evalStack.pop();
            }
            if (!evalStack.isEmpty()) {
                l = evalStack.pop();
            }
        } else if (arity == 1) {
            if (!evalStack.isEmpty()) {
                l = evalStack.pop();
            }
        }

        Operation.Operator op = operation.getOperator();

        // Vars for casting
        Integer left, right;
        Boolean lft, rght;

        // test sat
        switch (op) {
            case MUL:
                left = (Integer) l;
                right = (Integer) r;

                evalStack.push(left * right);
                break;
            case ADD:
                left = (Integer) l;
                right = (Integer) r;

                evalStack.push(left + right);
                break;
            case SUB:
                left = (Integer) l;
                right = (Integer) r;

                evalStack.push(left - right);
                break;
            case LE:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left <= right);
                evalStack.push(SAT);
                break;
            case OR:
                lft = (Boolean) l;
                rght = (Boolean) r;

                SAT = (lft || rght);
                evalStack.push(SAT);
                break;
            case AND:
                lft = (Boolean) l;
                rght = (Boolean) r;

                SAT = (lft && rght);
                evalStack.push(SAT);
                break;
            case EQ:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left == right);
                evalStack.push(SAT);
                break;
            case GE:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left >= right);
                evalStack.push(SAT);
                break;
            case LT:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left < right);
                evalStack.push(SAT);
                break;
            case NE:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left != right);
                evalStack.push(SAT);
                break;
            case GT:
                left = (Integer) l;
                right = (Integer) r;

                SAT = (left > right);
                evalStack.push(SAT);
                break;
            default:
                break;
        }
    }
}
