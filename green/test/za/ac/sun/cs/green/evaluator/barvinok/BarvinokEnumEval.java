package za.ac.sun.cs.green.evaluator.barvinok;

import org.apfloat.Apint;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import za.ac.sun.cs.green.EntireSuite;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.IntVariable;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @date: 2017/07/26
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 * Supervisor: Jaco Geldenhuys
 *
 * Description:
 * Evaluation of BarvinokEnumerateService.
 */
public class BarvinokEnumEval {
    public static Green solver = null;

    @BeforeClass
    public static void initialize() {
        solver = new Green();
        Properties props = new Properties();

        props.setProperty("green.services", "count");
        props.setProperty("green.service.count", "barvinok");
//        props.setProperty("green.service.count", "(canonize barvinok)");
//        props.setProperty("green.service.count.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
//        props.setProperty("green.service.count.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
        props.setProperty("green.service.count.barvinok", "za.ac.sun.cs.green.service.barvinok.BarvinokEnumerateService");

        String barvinokPath;
        if ((barvinokPath = EntireSuite.BARVINOK_ENUM_PATH) == null) {
            InputStream is = BarvinokEnumEval.class.getClassLoader()
                    .getResourceAsStream("green/build.properties");
            if (is != null) {
                Properties p = new Properties();
                try {
                    p.load(is);
                    barvinokPath = p.getProperty("isccpath");
                } catch (IOException e) {
                    // do nothing
                }
            }
        }

        props.setProperty("green.barvinok.path", barvinokPath);
        props.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
        Configuration config = new Configuration(solver, props);
        config.configure();
    }

    @AfterClass
    public static void report() {
        if (solver != null) {
            solver.report();
        }
    }

    private void check(Expression expression, Expression parentExpression, Apint expected) {
        Instance p = (parentExpression == null) ? null : new Instance(solver, null, parentExpression);
        Instance i = new Instance(solver, p, expression);
        Object result = i.request("count");
        assertNotNull(result);
        assertEquals(Apint.class, result.getClass());
        assertEquals(expected, result);
    }

    private void check(Expression expression, Apint expected) {
        check(expression, null, expected);
    }

    /*
     * Problem:
     * x > y =>
     * 1*x + -1 * y > 0
     * 1*x + -99 <= 0
     * 1*x >= 0
     * 1*y + -99 <= 0
     * 1*y >= 0
    */
    @Test
    public void test01() {
        IntVariable x = new IntVariable("x", 0, 99);
        IntVariable y = new IntVariable("y", 0, 99);

        Operation o1 = new Operation(Operation.Operator.GT, x, y);
        check(o1, new Apint(4950));
    }

    /*
     * Problem:
     * x > y =>
     * 1*x + -1 * y > 0
     * 1*x + -9 <= 0
     * 1*x >= 0
     * 1*y + -9 <= 0
     * 1*y >= 0
    */
    @Test
    public void test02() {
        IntVariable x = new IntVariable("x", 0, 9);
        IntVariable y = new IntVariable("y", 0, 9);

        Operation o1 = new Operation(Operation.Operator.GT, x, y);
        check(o1, new Apint(45));
    }
}
