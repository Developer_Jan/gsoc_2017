package za.ac.sun.cs.green.evaluator.barvinok;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Properties;

import org.apfloat.Apint;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import za.ac.sun.cs.green.EntireSuite;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.IntConstant;
import za.ac.sun.cs.green.expr.IntVariable;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

public class LattECountEval {

    public static Green solver = null;

    @BeforeClass
    public static void initialize() {
        if (!EntireSuite.HAS_LATTE) {
            Assume.assumeTrue(false);
            return;
        }
        solver = new Green();
        Properties props = new Properties();
        props.setProperty("green.services", "count");
        props.setProperty("green.service.count", "latte");
//        props.setProperty("green.service.count.bounder", "za.ac.sun.cs.green.service.bounder.BounderService");
        props.setProperty("green.service.count.latte",
                "za.ac.sun.cs.green.service.latte.CountLattEService");
        props.setProperty("green.latte.path", EntireSuite.LATTE_PATH);
        props.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
        Configuration config = new Configuration(solver, props);
        config.configure();
    }

    @AfterClass
    public static void report() {
        if (solver != null) {
            solver.report();
        }
    }

    private void check(Expression expression, Expression parentExpression, Apint expected) {
        Instance p = (parentExpression == null) ? null : new Instance(solver, null, parentExpression);
        Instance i = new Instance(solver, p, expression);
        Object result = i.request("count");
        assertNotNull(result);
        assertEquals(Apint.class, result.getClass());
        assertEquals(expected, result);
    }

    private void check(Expression expression, Apint expected) {
        check(expression, null, expected);
    }

    /*
     * Problem:
     * x > y =>
     * 1*x + -1 * y > 0
     * 1*x + -99 <= 0
     * 1*x >= 0
     * 1*y + -99 <= 0
     * 1*y >= 0
    */
    @Test
    public void test01() {
        IntVariable x = new IntVariable("x", 0, 99);
        IntVariable y = new IntVariable("y", 0, 99);
        IntConstant c1 = new IntConstant(1);
        IntConstant c2 = new IntConstant(-1);
        IntConstant max = new IntConstant(-99);

        Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
        Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
        Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
        Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

        Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
        Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

        Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
        Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
        Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

        Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

        Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
        Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

        Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
        Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
        Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
        Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
        check(o14, new Apint(4950));
    }

    /*
     * Problem:
     * x > y =>
     * 1*x + -1 * y > 0
     * 1*x + -9 <= 0
     * 1*x >= 0
     * 1*y + -9 <= 0
     * 1*y >= 0
    */
    @Test
    public void test02() {
        IntVariable x = new IntVariable("x", 0, 9);
        IntVariable y = new IntVariable("y", 0, 9);
        IntConstant c1 = new IntConstant(1);
        IntConstant c2 = new IntConstant(-1);
        IntConstant max = new IntConstant(-9);

        Operation v1 = new Operation(Operation.Operator.MUL, c1, x);
        Operation v2 = new Operation(Operation.Operator.MUL, c2, y);
        Operation o1 = new Operation(Operation.Operator.ADD, v1, v2);
        Operation o2 = new Operation(Operation.Operator.GT, o1, new IntConstant(0));

        Operation o3 = new Operation(Operation.Operator.ADD, v1, max);
        Operation o4 = new Operation(Operation.Operator.LE, o3, new IntConstant(0));

        Operation o5 = new Operation(Operation.Operator.MUL, c1, y);
        Operation o6 = new Operation(Operation.Operator.ADD, o5, max);
        Operation o7 = new Operation(Operation.Operator.LE, o6, new IntConstant(0));

        Operation o8 = new Operation(Operation.Operator.GE, v1, new IntConstant(0));

        Operation o9 = new Operation(Operation.Operator.MUL, c1, y);
        Operation o10 = new Operation(Operation.Operator.GE, o9, new IntConstant(0));

        Operation o11 = new Operation(Operation.Operator.AND, o2, o4);
        Operation o12 = new Operation(Operation.Operator.AND, o11, o7);
        Operation o13 = new Operation(Operation.Operator.AND, o12, o8);
        Operation o14 = new Operation(Operation.Operator.AND, o13, o10);
        check(o14, new Apint(45));
    }
}
