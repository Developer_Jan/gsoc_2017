package za.ac.sun.cs.green.experiments;

import org.junit.AfterClass;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.parser.sexpr.LIAParser;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.Properties;

/**
 * @date: 2017/06/26
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Mentor: Willem Visser
 *
 * Description:
 * JUnit test for testing the LIA parser.
 */
public class LIAExperiment {
    public static Green solver;
    private final String DRIVE = new File("").getAbsolutePath() + "/green/";
    private final String SUBDIRS = "test/za/ac/sun/cs/green/experiments/data/";
    private final String PRE = DRIVE + SUBDIRS;
    private final String EXTENSION = ".sexpr";

    @BeforeClass
    public static void initialize() {
        solver = new Green();
        Properties props = new Properties();
        props.setProperty("green.services", "sat");
        props.setProperty("green.service.sat", "(canonize julia)");
        props.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
        props.setProperty("green.service.sat.julia", "za.ac.sun.cs.green.service.julia.JuliaService");

        Configuration config = new Configuration(solver, props);
        config.configure();
    }

    @AfterClass
    public static void report() {
        solver.report();
    }

    private void check(Expression expression) {
        Instance i = new Instance(solver, null, expression);
        Object result = i.request("sat");

        if ((Boolean) result) {
            System.out.println("The expression is: SAT.");
        } else {
            System.out.println("The expression is: UNSAT.");

        }
    }

    private void check(String input) {
        try {
            LIAParser parser = new LIAParser();
            Operation o = parser.parse(input);
            check(o);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readFile(String path) {
        String contents;
        String absPath = PRE + path + EXTENSION;

        try {
            InputStream is = new FileInputStream(absPath);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();

            while(true){
                sb.append(line).append("\n");
                line = buf.readLine();

                if (line == null) {
                    break;
                }

                if (line.trim().isEmpty()) {
                    contents = sb.toString();
                    check(contents);
                    sb = new StringBuilder();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void testAll() {
        afsTest();
        avlTest();
        ballTest();
        blockTest();
        cdaudioTest();
        collisionTest();
        dijkstraTest();
        diskperfTest();
        divisionTest();
        floppyTest();
        grepTest();
        kbfiltrTest();
        knapsackTest();
        listTest();
        multiplicationTest();
        new_taxTest();
        old_taxTest();
        reversewordTest();
        tcasTest();
        swapwordsTest();
        treemapTest();
        wbsTest();
    }

    @Test
    public void afsTest()  {
        readFile("afs");
    }

    @Test
    public void avlTest() {
        readFile("avl");
    }

    @Test
    public void ballTest() {
        readFile("ball");
    }

    @Test
    public void blockTest() {
        readFile("block");
    }

    @Test
    public void cdaudioTest() {
        readFile("cdaudio");
    }

    @Test
    public void collisionTest() {
        readFile("collision");
    }

    @Test
    public void dijkstraTest() {
        readFile("dijkstra");
    }

    @Test
    public void diskperfTest() {
        readFile("diskperf");
    }

    @Test
    public void divisionTest() {
        readFile("division");
    }

    @Test
    public void floppyTest() {
        readFile("floppy");
    }

    @Test
    public void grepTest() {
        readFile("grep");
    }

    @Test
    public void kbfiltrTest() {
        readFile("kbfiltr");
    }

    @Test
    public void knapsackTest() {
        readFile("knapsack");
    }

    @Test
    public void listTest() {
        readFile("list");
    }

    @Test
    public void multiplicationTest() {
        readFile("multiplication");
    }

    @Test
    public void new_taxTest() {
        readFile("new-tax");
    }

    @Test
    public void old_taxTest() {
        readFile("old-tax");
    }

    @Test
    public void reversewordTest() {
        readFile("reverseword");
    }

    @Test
    public void swapwordsTest() {
        readFile("swapwords");
    }

    @Test
    public void tcasTest() {
        readFile("tcas");
    }

    @Test
    public void treemapTest()  {
        readFile("treemap");
    }

    @Test
    public void wbsTest()  {
        readFile("wbs");
    }

}
