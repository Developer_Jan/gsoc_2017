package za.ac.sun.cs.green.experiments;

import org.junit.BeforeClass;
import org.junit.Test;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.parser.sexpr.LIAParser;
import za.ac.sun.cs.green.util.Configuration;

import java.io.*;
import java.util.Properties;

/**
 * @date: 2017/07/27
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Project
 *
 * Description:
 * Examples for Demo
 */
public class Demo {
    private static Green Julia;
    private static Green JuliaFactor;
    private static Green JuliaCanon;
    private static Green JuliaFactorCanon;
    private static Green GREEN;

    private final String DRIVE = new File("").getAbsolutePath() + "/green/";
    private final String SUBDIRS = "test/za/ac/sun/cs/green/experiments/data/";
    private final String PRE = DRIVE + SUBDIRS;
    private final String EXTENSION = ".sexpr";

    @BeforeClass
    public static void initialize() {
        // Init plain Julia
        Julia = new Green();
        Properties props1 = new Properties();
        props1.setProperty("green.services", "sat");
        props1.setProperty("green.service.sat", "(julia)");
        props1.setProperty("green.service.sat.julia", "za.ac.sun.cs.green.service.julia.JuliaService");
        Configuration config1 = new Configuration(Julia, props1);
        config1.configure();

        // Init Julia Factor
        JuliaFactor = new Green();
        Properties props2 = new Properties();
        props2.setProperty("green.services", "sat");
        props2.setProperty("green.service.sat", "(factor julia)");
        props2.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
        props2.setProperty("green.service.sat.julia", "za.ac.sun.cs.green.service.julia.JuliaService");
        Configuration config2 = new Configuration(JuliaFactor, props2);
        config2.configure();

        // Init Julia Canon
        JuliaCanon = new Green();
        Properties props3 = new Properties();
        props3.setProperty("green.services", "sat");
        props3.setProperty("green.service.sat", "(canonize julia)");
        props3.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
        props3.setProperty("green.service.sat.julia", "za.ac.sun.cs.green.service.julia.JuliaService");
        Configuration config3 = new Configuration(JuliaCanon, props3);
        config3.configure();

        // Init Julia Factor Canon
        JuliaFactorCanon = new Green();
        Properties props5 = new Properties();
        props5.setProperty("green.services", "sat");
        props5.setProperty("green.service.sat", "(factor (canonize julia))");
        props5.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
        props5.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
        props5.setProperty("green.service.sat.julia", "za.ac.sun.cs.green.service.julia.JuliaService");
        Configuration config5 = new Configuration(JuliaFactorCanon, props5);
        config5.configure();

        // Init GREEN
        GREEN = new Green();
        Properties props4 = new Properties();
        props4.setProperty("green.services", "sat");
        props4.setProperty("green.service.sat", "(factor (canonize z3))");
        props4.setProperty("green.service.sat.factor", "za.ac.sun.cs.green.service.factorizer.SATFactorizerService");
        props4.setProperty("green.service.sat.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService");
        props4.setProperty("green.service.sat.z3", "za.ac.sun.cs.green.service.z3.SATZ3Service");
        props4.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
        Configuration config4 = new Configuration(GREEN, props4);
        config4.configure();
    }

    private void check(Expression expression, Green solver) {
        Instance i = new Instance(solver, null, expression);
        Object result = i.request("sat");

        if ((Boolean) result) {
            System.out.println("The expression is: SAT.");
        } else {
            System.out.println("The expression is: UNSAT.");

        }
    }

    private void check(String input, Green solver) {
        try {
            LIAParser parser = new LIAParser();
            Operation o = parser.parse(input);
            check(o, solver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readFile(String path, Green solver) {
        String contents;
        String absPath = PRE + path + EXTENSION;

        try {
            InputStream is = new FileInputStream(absPath);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();

            while(true){
                sb.append(line).append("\n");
                line = buf.readLine();

                if (line == null) {
                    break;
                }

                if (line.trim().isEmpty()) {
                    contents = sb.toString();
                    check(contents, solver);
                    sb = new StringBuilder();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJulia() {
        testAll(Julia);
    }

    @Test
    public void testJuliaFactor() {
        testAll(JuliaFactor);
    }

    @Test
    public void testJuliaCanon() {
        testAll(JuliaCanon);
    }

    @Test
    public void testJuliaFactorCanon() {
        testAll(JuliaFactorCanon);
    }

    @Test
    public void testGreen() {
        testAll(GREEN);
    }

    public void testAll(Green solver) {
        afsTest(solver);
        tcasTest(solver);
    //        treemapTest(solver);
        wbsTest(solver);
        solver.report();
    }

    private void afsTest(Green solver)  {
        readFile("afs", solver);
    }

    private void tcasTest(Green solver) {
        readFile("tcas", solver);
    }

    private void treemapTest(Green solver)  {
        readFile("treemap", solver);
    }

    private void wbsTest(Green solver)  {
        readFile("wbs", solver);
    }

}
