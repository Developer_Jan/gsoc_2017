package za.ac.sun.cs.green.service.propagation;

import org.junit.BeforeClass;
import org.junit.Test;
import za.ac.sun.cs.green.Green;
import za.ac.sun.cs.green.Instance;
import za.ac.sun.cs.green.expr.Expression;
import za.ac.sun.cs.green.expr.IntConstant;
import za.ac.sun.cs.green.expr.IntVariable;
import za.ac.sun.cs.green.expr.Operation;
import za.ac.sun.cs.green.util.Configuration;

import java.util.Arrays;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @date: 2017/04/30
 * @author: JH Taljaard.
 * Student Number: 18509193.
 * Course: CS778
 *
 * Description: UnitPropagationTest
 * {Description here}
 */
public class UnitPropagationTest {

    public static Green solver;

    @BeforeClass
    public static void initialize() {
        solver = new Green();
        Properties props = new Properties();
        props.setProperty("green.services", "sat");
        props.setProperty("green.service.sat", "(slice (propagate sink))");
        props.setProperty("green.service.sat.slice",
                "za.ac.sun.cs.green.service.slicer.SATSlicerService");
        props.setProperty("green.service.sat.propagate",
                "za.ac.sun.cs.green.service.propagation.UnitPropagation");
        props.setProperty("green.service.sat.sink",
                "za.ac.sun.cs.green.service.sink.SinkService");
        Configuration config = new Configuration(solver, props);
        config.configure();
    }

    private void finalCheck(String observed, String[] expected) {
        String s0 = observed.replaceAll("[()]", "");
        String s1 = s0.replaceAll("v[0-9]", "v");
        SortedSet<String> s2 = new TreeSet<String>(Arrays.asList(s1.split("&&")));
        SortedSet<String> s3 = new TreeSet<String>(Arrays.asList(expected));
        assertEquals(s2, s3);
    }

    private void check(Expression expression, String full,
                       String... expected) {
        Instance i = new Instance(solver, null, null, expression);
        Expression e = i.getExpression();
        assertTrue(e.equals(expression));
        assertEquals(expression.toString(), e.toString());
        assertEquals(full, i.getFullExpression().toString());
        Object result = i.request("sat");
        assertNotNull(result);
        assertEquals(Instance.class, result.getClass());
        Instance j = (Instance) result;
        finalCheck(j.getExpression().toString(), expected);
    }

    private void check(Expression expression, Expression parentExpression,
                       String full, String... expected) {
        Instance i1 = new Instance(solver, null, parentExpression);
        Instance i2 = new Instance(solver, i1, expression);
        Expression e = i2.getExpression();
        assertTrue(e.equals(expression));
        assertEquals(expression.toString(), e.toString());
        assertEquals(full, i2.getFullExpression().toString());
        Object result = i2.request("sat");
        assertNotNull(result);
        assertEquals(Instance.class, result.getClass());
        Instance j = (Instance) result;
        finalCheck(j.getExpression().toString(), expected);
    }

    @Test
    public void test01() {
        // x = 0
        IntVariable v1 = new IntVariable("x", 0, 99);
        IntConstant c = new IntConstant(0);
        Operation o1 = new Operation(Operation.Operator.EQ, v1, c);
        check(o1, "x==0", "1*v==0");
    }

    @Test
    public void test02() {
        // x = 0 & y = x
        IntVariable v1 = new IntVariable("x", 0, 99);
        IntConstant c = new IntConstant(0);
        IntVariable v2 = new IntVariable("y", 0, 99);
        Operation o1 = new Operation(Operation.Operator.EQ, v1, c);
        Operation o2 = new Operation(Operation.Operator.EQ, v2, v1);
        Operation o3 = new Operation(Operation.Operator.AND, o1, o2);
        check(o3, "(x==0)&&(y==x)", "1*v==0", "1*v+-1*v==0");
    }

    @Test
    public void test03() {
        // x = 1 & y = x & z+1 = 0
        IntVariable v1 = new IntVariable("x", 0, 99);
        IntConstant c = new IntConstant(1);
        IntVariable v2 = new IntVariable("y", 0, 99);
        Operation o1 = new Operation(Operation.Operator.EQ, v1, c);
        Operation o2 = new Operation(Operation.Operator.EQ, v2, v1);
        Operation o3 = new Operation(Operation.Operator.AND, o1, o2);
        IntVariable v3 = new IntVariable("z", 0, 99);
        Operation o4 = new Operation(Operation.Operator.ADD, v3, c);
        Operation o5 = new Operation(Operation.Operator.AND, o3, o4);
        check(o5, "((x==1)&&(y==x))&&(z+1)", "1*v+-1==0", "1*v+-1*v==0");
    }

    @Test
    public void test04() {
        // x = 0 & y = x & z > 0
        IntVariable v1 = new IntVariable("x", 0, 99);
        IntConstant c = new IntConstant(0);
        IntVariable v2 = new IntVariable("y", 0, 99);
        Operation o1 = new Operation(Operation.Operator.EQ, v1, c);
        Operation o2 = new Operation(Operation.Operator.EQ, v2, v1);
        Operation o3 = new Operation(Operation.Operator.AND, o1, o2);
        IntVariable v3 = new IntVariable("z", 0, 99);
        Operation o4 = new Operation(Operation.Operator.GT, v3, c);
        Operation o5 = new Operation(Operation.Operator.AND, o3, o4);
        check(o5, "((x==0)&&(y==x))&&(z>0)", "1*v==0", "1*v+-1*v==0", "-1*v+1<=0");
    }

}